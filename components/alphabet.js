'use strict';

import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  Alert,
  TouchableOpacity
} from 'react-native';

var styles = StyleSheet.create({
  textAlphabet: {
    fontSize: 300,
    textAlign: 'center',
    color: 'white',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    marginTop: 65,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'skyblue',
  }
});

var alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",];

export default class Alphabet extends Component {

  constructor(props) {
    super(props);

    this.state = {
      letter: alphabet[0],
      i: 1
    }

    this.pressScreen = this.pressScreen.bind(this);
  };

  pressScreen() {
      if (this.state.i < 26) {
        this.setState({
          i: this.state.i + 1,
          letter: alphabet[this.state.i]
        })
      } else {
        this.setState({
          i: 1,
          letter: alphabet[0]
        })
      }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity 
        style={styles.button}
        onPress={this.pressScreen}>
            <Text style={styles.textAlphabet}>{this.state.letter}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}